# Serial Port to TCP/IP Communication bridge

This project aims to give a basic and easy way to connect a microcontroller (as Arduino) through the Serial Port of a PC to a TCP/IP Server.

This program was first made for [GQRX-physical-control](https://gitlab.com/pan__duro/gqrx-physical-control) by Fernado Filippetti.

## Build & install

You can install the binary file in your system following this simple steps:

```bash
wget https://gitlab.com/tute-avalos/serial-tcp/uploads/77493dd995ad1191ac80d477b494be28/serial-tcp-0.1.tar.gz -O - | tar -xz && cd serial-tcp-0.1
./configure
make
sudo make install # optional
```

Now you cant type `serial-tcp` in a terminal and it will prompt the usage.

## Usage

The usage it's quite simple:

```bash
serial-tcp <serial-device> <baudrate> <server-ip> <port>
```

For example:

```bash
serial-tcp /dev/ttyUSB0 9600 127.0.0.1 7356
```

## TODO

* Add debian packaging
* Add I18n w/gettext (there is no much text around)
* Add **binary** communication some how
